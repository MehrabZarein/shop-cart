import React, { useContext } from "react";
import CartContext from "./CartContext";

import { Link } from "react-router-dom";
function ShopCart(props) {
  const [cart, setCart] = useContext(CartContext);

  return (
    <div>
      <nav>
        <ul>
          <li>
            <div>
              <Link to="/basket" className="btn btn-primary">
                items in cart : {cart.length}
              </Link>
            </div>
          </li>
        </ul>
      </nav>
    </div>
  );
}

export default ShopCart;
