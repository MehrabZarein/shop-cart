import React from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Products from "../Products/Products";
import BasketWrapper from "../Basket/BasketWrapper";

const App = () => {
  return (
    <Router>
      <div className="rtl">
        <Switch>
          <Route path="/" exact component={Products} />
          <Route path="/basket" component={BasketWrapper} />
        </Switch>
      </div>
    </Router>
  );
};

export default App;
