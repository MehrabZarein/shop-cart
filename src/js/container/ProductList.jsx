import React from "react";
import { Product } from "./Product";
import lists from "../data";

export const ProductList = props => {
  return (
    <div className="container-fluid" style={{ backgroundColor: "whitesmoke" }}>
      <div className="row">
        {lists.map(item => (
          <Product name={item.name} price={item.price} img={item.img} key={item.id} />
        ))}
      </div>
    </div>
  );
};
