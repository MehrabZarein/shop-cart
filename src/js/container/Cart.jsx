import React, { useContext } from "react";
import CartContext from "./CartContext";
import { Link } from "react-router-dom";
import ShopCart from "./ShopCart";
export const Cart = () => {

  const [cart, setCart] = useContext(CartContext);
  const total = cart.reduce((acc, curr) => acc + curr.price, 0);
  const totalPrice = total.toLocaleString("fa-IR");
  return (
    <nav className="navbar text-white bg-dark p-3">
      <div className="container">
        <div className="d-flex justify-content-between align-items-center">
          <Link to="/cart">
            <ShopCart cart={cart}/>
          </Link>
        </div>
        <span>total price : {totalPrice} </span>
        {/* <pre>{JSON.stringify(cart , "\t\r" , 2)}</pre> */}
      </div>
    </nav>
  );
};
