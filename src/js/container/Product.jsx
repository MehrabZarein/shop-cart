import React, { useContext } from "react";
import CartContext from "./CartContext";

export const Product = props => {
  const [cart, setCart] = useContext(CartContext);
  const addToCart = () => {
    const product = { name: props.name, price: props.price };
    setCart(curr => [...curr, product]);
  };
  
  return (
    <div className="col-12 col-lg-2 col-md-4 col-sm-6 p-1 d-flex align-items-stertch">
      <div className="card rounded-0 w-100">
        <div className="card-body">
          <div className="card-img-top">
            <img
              className="img-fluid mb-2"
              style={{ width: "190px", height: "190px" }}
              src={props.img}
              alt={props.name}
            />
          </div>
          <span className="rtl">{props.name}</span>
          <div className="d-flex justify-content-between align-items-center mt-2">
            <div className="btn-click">

            </div>
            <div className="mr-1">
            <button
              className="btn btn-danger border-0 font-weeight-bold px-4"
              onClick={addToCart}

            >
              <span className="font-weight-bold"> + </span>
            </button>
            </div>
              <h5>{props.price.toLocaleString("fa-IR")} تومان </h5>
          </div>
        </div>
      </div>
    </div>
    
  );
};
