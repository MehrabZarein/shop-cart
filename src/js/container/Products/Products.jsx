import React from "react";
import { ProductList } from "../ProductList";
import { Cart } from "../Cart";
import { CartProvider } from "../CartContext";
// import ShopCart from "../ShopCart";

function Products() {
  return (
    <div>
      <CartProvider>
        <Cart />
        <ProductList />
      </CartProvider>
    </div>
  );
}

export default Products;
