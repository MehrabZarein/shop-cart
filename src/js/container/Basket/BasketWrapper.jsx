import React, { useContext } from "react";
import {CartProvider} from "../CartContext";
import Basket from "./Basket";

function BasketWrapper() {

  return (
    <CartProvider>
        <Basket />
    </CartProvider>
  );
}


export default BasketWrapper;
